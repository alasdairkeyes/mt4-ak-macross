# mt4-ak-macross

Metatrader4 EA (Expert Advisor) which opens/closes trades based off the cross of moving average lines.

## Outline

This EA tracks two moving averages, a `Fast` Moving average and a `Slow` Moving average.

## EA Settings

* **Lot**: The lot size to use when opening a position. (Default: 0.01)
* **SlowMAPeriod**: Set the period to use for the Slow Moving average line. This must be a higher number than the `FastMAPeriod` (Default: 50)
* **SlowMAMode**: Set the Moving Average Type. Options are
    * Simple (Default)
    * Exponential
    * Smoothed
    * Linear Weighted
* **SlowMAPrice**: Set the price which is used for calculating the Moving Average. Options are
    * Close (Default)
    * Open
    * High
    * Low
    * Median
    * Typical
    * Weighted
* **FastMAPeriod**: Set the period to use for the Fast Moving Average Line (Default: 20)
* **FastMAMode**: Set the Moving Average Type. Options are
    * Simple (Default)
    * Exponential
    * Smoothed
    * Linear Weighted
* **FastMAPrice**: Set the price which is used for calculating the Moving Average. Options are
    * Close (Default)
    * Open
    * High
    * Low
    * Median
    * Typical
    * Weighted
* **TakeProfitPoints**: The number of points to set Take Profit at when the position is opened. Metatrader4 deals in `points`. A pip is 10 points. When set to zero, take profit will not be set. One of either `TrailingStopPoints` or `TakeProfitPoints` must be set to greater than 0, both can be set.
* **StopLossPoints**: Set the number of points to set Stop Loss at when the position is opened. Metatrader4 deals in `points`. A pip is 10 points. When set to zero, Stop Loss will not be set
* **TrailingStopPoints**: Set the number of points to set a Trailing Stop Loss at. Metatrader deals in `points`. A pip is 10 points. When set to zero a Trailing Stop is not used. One of either `TrailingStopPoints` or `TakeProfitPoints` must be set to greater than 0, both can be set.
* **CloseOpenTradeWhenMACross**: When set to true, if a trade is currently open and the Moving Averages cross back indicating a movement in the opposite direction, the current trade will be closed.
* **Debug**: Debug option to display extra information on how the EA is operating. Options are
  * None (Default)
  * Basic
  * Full

## Notes and suggestions

* Always run this EA within a test or demo environment before running on a live account.
* No timeframes or currency pairs are recommended for use with this EA.
* Default values are not suitable. Try the Metatrader4 Optimization tool.

## Bugs

* Please report any bugs found.

## License

This is distributed under GPL v3.0. See the included `LICENSE` file.

## Notice and Warranty

The software is provided "as is", without warranty of any kind, express or implied.
