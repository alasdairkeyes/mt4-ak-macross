//+------------------------------------------------------------------+
//|                                               mt4-ak-macross.mq4 |
//|                                                   Alasdair Keyes |
//|                  https://gitlab.com/alasdairkeyes/mt4-ak-macross |
//+------------------------------------------------------------------+
#property copyright "Alasdair Keyes"
#property link      "https://gitlab.com/alasdairkeyes/mt4-ak-macross"
#property version   "1.00"
#property strict
#define MAGICMA 2349576

// Define Custom Debug Values/Enum
#define DEBUG_NONE   0
#define DEBUG_BASIC  1
#define DEBUG_FULL   2

enum DebugValues {
   None  = DEBUG_NONE,
   Basic = DEBUG_BASIC,
   Full  = DEBUG_FULL
};

// Input parameters
input double               Lot = 0.01;

input int                  SlowMAPeriod = 50;
input ENUM_MA_METHOD       SlowMAMode = MODE_EMA;
input ENUM_APPLIED_PRICE   SlowMAAppliedPrice = PRICE_CLOSE;

input int                  FastMAPeriod = 20;
input ENUM_MA_METHOD       FastMAMode = MODE_EMA;
input ENUM_APPLIED_PRICE   FastMAAppliedPrice = PRICE_CLOSE;

// Any of these set to 0 will mean they aren't used.
input int                  TakeProfitPoints = 300;
input int                  StopLossPoints = 150;
input int                  TrailingStopPoints = 0;

input bool                 CloseOpenTradeWhenMACross = true;

input DebugValues          Debug = 1;

// General Vars
string          EAName = "mt4-ak-macross";
datetime        LastRun;

// The following MA Parameters can be made into inputs but it will add
// complexity to new users.
// These are the 'ma_period' offset value for the iMA() function
int             SlowMAOffset = 0;
int             FastMAOffset = 0;

// This is the 'shift' value for the iMA() function
int             FirstCalculationPeriod    = 1;
int             SecondCalculationPeriod   = FirstCalculationPeriod + 1;

// Graph Colours
color           LongOrderOpenColour    = Blue;
color           ShortOrderOpenColour   = Red;
color           OrderCloseColour       = White;


int OnInit() {

   // Check that fast period value is faster than the slow period value
   if (FastMAPeriod >= SlowMAPeriod) {
      Print("FastMAPeriod must be less than SlowMAPeriod");
      return(INIT_PARAMETERS_INCORRECT);
   }

   // Check that we have some way of exiting the trade in profit
   if (TakeProfitPoints < 1 && TrailingStopPoints < 1) {
      Print("You must set either TakeProfitPoints or TrailingStopPoints to be greater than 0");
      return(INIT_PARAMETERS_INCORRECT);
   }

   return(INIT_SUCCEEDED);
}

/*
   Function uses the Time[0] variable to determine if it has been called again
   within the same period. This is more reliable than using Volume variable
*/
bool HaveWeRunThisPeriod() {
   if (LastRun && Time[0] == LastRun) {
      return true;
   }

   LastRun = Time[0];
   return false;
}

void OnDeinit(const int reason) {
}

/*
   Helper function for printing Debug based on Debug Level
*/
void Debug(int Level, string DebugMessage) {
   if (Debug >= Level)
      Print(DebugMessage);
}

/*
   Return dynamic comment for the order based on current Graph Period.
*/
string EAComment() {
   return EAName + " " + IntegerToString(Period());
}

// Helper Functions to generate Slow/Fast MA values.
double SlowMA1() {
   return iMA(
      NULL,
      PERIOD_CURRENT,
      SlowMAPeriod,
      SlowMAOffset,
      SlowMAMode,
      SlowMAAppliedPrice,
      FirstCalculationPeriod
   );
}

double SlowMA2() {
   return iMA(
      NULL,
      PERIOD_CURRENT,
      SlowMAPeriod,
      SlowMAOffset,
      SlowMAMode,
      SlowMAAppliedPrice,
      SecondCalculationPeriod
   );
}

double FastMA1() {
   return iMA(
      NULL,
      PERIOD_CURRENT,
      FastMAPeriod,
      FastMAOffset,
      FastMAMode,
      FastMAAppliedPrice,
      FirstCalculationPeriod
   );
}

double FastMA2() {
   return iMA(
      NULL,
      PERIOD_CURRENT,
      FastMAPeriod,
      FastMAOffset,
      FastMAMode,
      FastMAAppliedPrice,
      SecondCalculationPeriod
   );
}

/*
   Return true if FastMA line crosses Above SlowMA Line
*/
bool IsFastCrossingAboveSlow() {
   if (SlowMA1() < FastMA1() && SlowMA2() > FastMA2())
      return true;

   return false;
}

/*
   Return true if FastMA line crosses Below SlowMA Line
*/
bool IsFastCrossingBelowSlow() {
   if (SlowMA1() > FastMA1() && SlowMA2() < FastMA2())
      return true;

   return false;
}

/*
   OnTick() function to run every tick
*/
void OnTick() {

   if (IsTradeAllowed() == false)
      return;

   if(Bars < SlowMAPeriod)
      return;

   if (HaveWeRunThisPeriod())
      return;

   // Some useful debugging
   Debug(
       DEBUG_FULL,
       StringFormat(
           "SlowMA1: %f SlowMA2: %f FastMA1: %f FastMA2: %f CurrentSpread: %.0f (%f)",
           SlowMA1(),
           SlowMA2(),
           FastMA1(),
           FastMA2(),
           (Ask - Bid) / Point,
           Ask - Bid
       )
   );
   if (IsFastCrossingAboveSlow())
      Debug(DEBUG_BASIC, "FastMA is crossing above SlowMA");
   if (IsFastCrossingBelowSlow())
      Debug(DEBUG_BASIC, "FastMA is crossing below SlowMA");

   // Do the work
   CloseOpenOrdersOnCross();
   AdjustTrailingStopOnOpenOrders();
   CheckForOpen();
}

/*
   Cycle through orders and adjust the Trailing Stop if required
*/
void AdjustTrailingStopOnOpenOrders() {

   if (TrailingStopPoints < 1)
      return;

   for(int i=0; i<OrdersTotal(); i++) {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES) == false)
         break;

      if(OrderMagicNumber() != MAGICMA || OrderSymbol() != Symbol())
         continue;

      if(OrderType() == OP_BUY) {
         double NewStopLoss = GenerateLongSLValue(TrailingStopPoints);
         if (NewStopLoss > OrderOpenPrice()) {
            if (OrderStopLoss() < NewStopLoss) {
               Debug(
                  DEBUG_BASIC,
                  StringFormat(
                     "Changing StopLoss on %d from %f to %f",
                     OrderTicket(),
                     OrderStopLoss(),
                     NewStopLoss
                  )
               );

               int res = OrderModify(
                  OrderTicket(),
                  OrderOpenPrice(),
                  NewStopLoss,
                  OrderTakeProfit(),
                  0,
                  LongOrderOpenColour
               );

               if(!res)
                  Print("OrderModify error ",GetLastError());

               return;
            }
         }
      }

      if(OrderType() == OP_SELL) {
         double NewStopLoss = GenerateShortSLValue(TrailingStopPoints);
         if (OrderOpenPrice() > NewStopLoss) {
            if (OrderStopLoss() > NewStopLoss) {
               Debug(
                  DEBUG_BASIC,
                  StringFormat(
                     "Changing StopLoss on %d from %f to %f",
                     OrderTicket(),
                     OrderStopLoss(),
                     NewStopLoss
                  )
               );

               int res = OrderModify(
                  OrderTicket(),
                  OrderOpenPrice(),
                  NewStopLoss,
                  OrderTakeProfit(),
                  0,
                  ShortOrderOpenColour
               );

               if(!res)
                  Print("OrderModify error ",GetLastError());

               return;
            }
         }
      }
   }
}

/*
   Return a price value for a Take Profit based on current price and the provided
   Take Profit value in points
*/
double GenerateLongTPValue(double NewTPPoints) {
   return NormalizeDouble(Ask + NewTPPoints * Point, Digits);
}

/*
   Return a price value for a Stop Loss based on current price and the provided
   Stop Loss value in points
*/
double GenerateLongSLValue(double NewSLPoints) {
   return NormalizeDouble(Bid - Point * NewSLPoints, Digits);
}

/*
   Return a price value for a Take Profit based on current price and the provided
   Take Profit value in points
*/
double GenerateShortTPValue(double NewTPoints) {
   return NormalizeDouble(Bid - NewTPoints * Point, Digits);
}

/*
   Return a price value for a Stop Loss based on current price and the provided
   Stop Loss value in points
*/
double GenerateShortSLValue(double NewSLPoints) {
   return NormalizeDouble(Ask + Point * NewSLPoints, Digits);
}

/*
   Check if the rules are right to open a trade
*/
void CheckForOpen() {

   // Don't open if we already have an order open
   if (HaveOpenOrder())
      return;

   int res;

   // Create Long
   if (IsFastCrossingAboveSlow()) {
      Debug(DEBUG_BASIC,"Going Long");

      double OrderTP = TakeProfitPoints > 0
         ? GenerateLongTPValue(TakeProfitPoints)
         : 0;
      double OrderSL = StopLossPoints
         ? GenerateLongSLValue(StopLossPoints)
         : 0;
      res=OrderSend(
          Symbol(),
          OP_BUY,
          Lot,
          Ask,
          3,
          OrderSL,
          OrderTP,
          EAComment(),
          MAGICMA,
          0,
          LongOrderOpenColour
      );
      return;
   }

   // Create short
   if (IsFastCrossingBelowSlow()) {
      Debug(DEBUG_BASIC,"Going Short");

      double OrderTP = TakeProfitPoints
         ? GenerateShortTPValue(TakeProfitPoints)
         : 0;
      double OrderSL = StopLossPoints
         ? GenerateShortSLValue(StopLossPoints)
         : 0;
      res=OrderSend(
          Symbol(),
          OP_SELL,
          Lot,
          Bid,
          3,
          OrderSL,
          OrderTP,
          EAComment(),
          MAGICMA,
          0,
          ShortOrderOpenColour
      );
      return;
   }

}

/*
   Closes any open orders that exist for this EA/symbol if the price is
   crossing and the CloseOpenTradeWhenMACross is true
*/
void CloseOpenOrdersOnCross() {

   if (CloseOpenTradeWhenMACross == false)
      return;

   for(int i=0;i<OrdersTotal();i++) {
      if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false)
         break;

      if(OrderMagicNumber()!=MAGICMA || OrderSymbol()!=Symbol())
         continue;

      if(OrderType()==OP_BUY) {
         if(IsFastCrossingBelowSlow()) {
            Debug(DEBUG_BASIC, StringFormat("Closing open long order %d", OrderTicket()));

            if(!OrderClose(OrderTicket(),OrderLots(),Bid,3,OrderCloseColour))
               Print("OrderClose error ",GetLastError());

            break;
         }
      }

      if(OrderType()==OP_SELL) {
         if(IsFastCrossingAboveSlow()) {
            Debug(DEBUG_BASIC, StringFormat("Closing open short order %d", OrderTicket()));

            if(!OrderClose(OrderTicket(),OrderLots(),Ask,3,OrderCloseColour))
               Print("OrderClose error ",GetLastError());
            break;
         }
      }
   }
}

/*
   Returns true if this EA has any open orders on this symbol
*/
bool HaveOpenOrder() {
    for(int i=0;i<OrdersTotal();i++) {
        if(OrderSelect(i,SELECT_BY_POS,MODE_TRADES)==false)
            break;

        if(OrderMagicNumber()!=MAGICMA || OrderSymbol()!=Symbol())
            continue;

        return true;
    }

    return false;
}
